//Imprimir el promedio de las calificaciones
function imprimirPromedio(){
    var tabla_calificaciones = document.getElementById('tabla_calificaciones');
    var tabla_body = tabla_calificaciones.lastElementChild;
    var total = 0;
    for (var i = 0; i <tabla_body.children.length; i++) {
        total += Number(tabla_body.children[i].children[2].textContent);
    }
    var li_promedio = document.getElementById('promedio');
    var promedio = total / tabla_body.children.length;
    li_promedio.textContent = "Promedio: " +  promedio;
}
imprimirPromedio();

//Imprimir la materia con mejor calificacion
var califMayor = (function calcMayor()
{
    var tabla_calificaciones = document.getElementById('tabla_calificaciones');
    var tabla_body = tabla_calificaciones.children[1];
    
    var c1 = 0;
    var c2 = 0;
    
    for (var i = 0; i<tabla_body.children.length; i++) {
        c1 = parseInt(tabla_body.children[i].children[2].textContent);
        
        if(c1 > c2) {
            var mayor = c1;
            c2 = c1;
            
            nombre = (tabla_body.children[i].children[1].textContent);
            clave = (tabla_body.children[i].children[0].textContent);
        }
    }
    return "(" + clave + ") " + nombre;
});

mejor.textContent= "Materia con mejor calificacion: " + califMayor();

//Imprimir la materia con menor calificacion
var califMenor = (function calcMenor()
{
    var tabla_calificaciones = document.getElementById('tabla_calificaciones');
    var tabla_body = tabla_calificaciones.children[1];
    
    var c1 = 0;
    var c2 = 100;
    
    for (var i = 0; i < tabla_body.children.length; i++) {
        c1 = parseInt (tabla_body.children[i].children[2].textContent);
        if(c1 < c2) {
            var menor = c1;
            c2 = c1;
            nombre = (tabla_body.children[i].children[1].textContent);
            clave = (tabla_body.children[i].children[0].textContent);
        }
    }
    return "(" + clave + ") " + nombre;
});

menor.textContent = "Materia con menor calificacion: " + califMenor();